# knowledge-base-知识库系统

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
# JWT请求
https://developer.aliyun.com/mvn/guide

通过用户名密码异步请求获取token

```cmd
http://localhost:8082/user/login?client_name=rest&username=admin&password=123
```
返回的json
```json
{
    "token": "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0..zDvUm-Q9YhwdvcR1.fz5-ar7FEWEnisDjyjZZ5lgb8xWaS5sffOafYUeZ_-sJJLSx2utoDOK2d1gS8G2oYbg7kbSR_Pcb_m3x3a_awLsoki79LOtk-yI1INWcYJTuF_zY27JPIqAOXF8GFwIc0QzuPke6mLoVUzuwc7ILjvqXg9Z2VA7hWrYJZ1WNsO3UDyPAltq9TXgzl3aJc3XBAUXPNzw8BLUDclTUGs1MnNzjlZYI94qVZgsybZwokkXh1WZ8JEnc7XGtFNJVQOHiIbhSCYJgkjb5xjEtZRRbI7LGSn-kPm99tau68hrR3qbcZofR3lxZ8p-Cta9EHiZ-SQGFg6yBNFSslQnNSoYPuyOo4kFtUgUvG9QJJgwMj8E_sXnixJ4rBkcLuok7mBvkelYr8CbBUyMdZJvwhPCEt9yaBDxJSGL-osWhvseoFFVc9Rp-Egie-NCuUzrygzi2juwMyLLsHybQL6m77rn3Hi_flgAtClUeyuwLbCSQ_Qn2fSz31NCydkxsC7NIsMf-VDiQwQMe9eO3WLcAkr1EAUCyMse61eww9n944350oXhMAQLpShr45AEXNHQ7wls5ZS1Wh6llm4kpQyMh_fwfNEAS7bPBlncKc9rjMloYaQPIkshxh5qysVxPlmWSctGxDdQMzQoJWGaeA9ZXSivF0p_zDAAqM0PAwUPYlBlRkCbO-2I7fapCs-5r0zpZBvYCwYxr4-h1n98x.v8ucroIDOmJbMLLwUkLUow"
}
```

通过token访问资源
```cmd
http://localhost:8082/user/detail?client_name=jwt&token=eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0..zDvUm-Q9YhwdvcR1.fz5-ar7FEWEnisDjyjZZ5lgb8xWaS5sffOafYUeZ_-sJJLSx2utoDOK2d1gS8G2oYbg7kbSR_Pcb_m3x3a_awLsoki79LOtk-yI1INWcYJTuF_zY27JPIqAOXF8GFwIc0QzuPke6mLoVUzuwc7ILjvqXg9Z2VA7hWrYJZ1WNsO3UDyPAltq9TXgzl3aJc3XBAUXPNzw8BLUDclTUGs1MnNzjlZYI94qVZgsybZwokkXh1WZ8JEnc7XGtFNJVQOHiIbhSCYJgkjb5xjEtZRRbI7LGSn-kPm99tau68hrR3qbcZofR3lxZ8p-Cta9EHiZ-SQGFg6yBNFSslQnNSoYPuyOo4kFtUgUvG9QJJgwMj8E_sXnixJ4rBkcLuok7mBvkelYr8CbBUyMdZJvwhPCEt9yaBDxJSGL-osWhvseoFFVc9Rp-Egie-NCuUzrygzi2juwMyLLsHybQL6m77rn3Hi_flgAtClUeyuwLbCSQ_Qn2fSz31NCydkxsC7NIsMf-VDiQwQMe9eO3WLcAkr1EAUCyMse61eww9n944350oXhMAQLpShr45AEXNHQ7wls5ZS1Wh6llm4kpQyMh_fwfNEAS7bPBlncKc9rjMloYaQPIkshxh5qysVxPlmWSctGxDdQMzQoJWGaeA9ZXSivF0p_zDAAqM0PAwUPYlBlRkCbO-2I7fapCs-5r0zpZBvYCwYxr4-h1n98x.v8ucroIDOmJbMLLwUkLUow
```
返回

```text
users:admin
```
