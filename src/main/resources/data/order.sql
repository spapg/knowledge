create table `order` (
  id             integer not null auto_increment,
  adress         varchar(255),
  amount         varchar(255),
  book_id        integer,
  book_name      varchar(255),
  book_num       integer,
  date           varchar(255),
  express        varchar(255),
  order_id       varchar(255),
  payment_method varchar(255),
  purchaser_id   integer,
  status         varchar(255),
  primary key (id)
)
