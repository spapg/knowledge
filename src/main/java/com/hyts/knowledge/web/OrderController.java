package com.hyts.knowledge.web;

import com.hyts.knowledge.dao.OrderMapper;
import com.hyts.knowledge.dao.ReaderMapper;
import com.hyts.knowledge.model.Order;
import com.hyts.knowledge.model.Reader;
import com.hyts.knowledge.model.ResultMap;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 *
 * @Author yuanhaoyue swithaoy@gmail.com
 * @Description
 * @Date 2019-01-02
 * @Time 13:28
 */
@RestController
public class OrderController {
    private final ResultMap resultMap;
    private final OrderMapper orderMapper;
    private final ReaderMapper readerMapper;

    @Autowired
    public OrderController(ResultMap resultMap, OrderMapper orderMapper, ReaderMapper readerMapper) {
        this.resultMap = resultMap;
        this.orderMapper = orderMapper;
        this.readerMapper = readerMapper;
    }


    @RequestMapping(value = "/order", method = RequestMethod.GET)
    @RequiresRoles("admin")
    public ResultMap getOrders() {
        List<Order> list = orderMapper.getOrders();
        for (Order order : list) {
            Reader reader = readerMapper.getReader(order.getPurchaserId());
            order.setReader(reader);
        }
        return resultMap.success().message(list);
    }

    @RequestMapping(value = "/orderInformation/{id}", method = RequestMethod.GET)
    @RequiresRoles("admin")
    public ResultMap getOrderInformation(@PathVariable("id") int id) {
        Order order = orderMapper.getOrderInformation(id);
        Reader reader = readerMapper.getReader(order.getPurchaserId());
        order.setReader(reader);
        return resultMap.success().message(order);
    }

}
